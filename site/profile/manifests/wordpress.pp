class profile::wordpress (

) {
    include wordpress
    include mysql::server
    include apache

    apache::vhost { $facts['fqdn']:
        port    => '8080',
        docroot => '/opt/wordpress',
    }

    include apache::mod::php
    
    package { 'php-mysqlnd':
        ensure => installed,
        notify => Service['httpd'],
     }
}